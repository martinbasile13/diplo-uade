CREATE SCHEMA IF NOT EXISTS `finhogar` DEFAULT CHARACTER SET utf8 ;
USE `finhogar` ;

CREATE TABLE IF NOT EXISTS `finhogar`.`ingresos` (
  `id` INT NOT NULL,
  `nombre` VARCHAR(20) NOT NULL,
  `tipo` VARCHAR(20) NOT NULL,
  `procedencia` VARCHAR(20) NOT NULL,
  `frecuencia` VARCHAR(20) NOT NULL,
  `monto` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;