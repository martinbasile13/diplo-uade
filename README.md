#Proyecto
Nombre del Proyecto FinHogar

Una web para el control de finanzas

#Repositorios
https://gitlab.com/martinbasile13/diplo-uade.git
https://gitlab.com/martinbasile13/finhogar-backend.git
https://gitlab.com/martinbasile13/finhogar-frontend.git
###################################

###Deploys

#Database
Para la base de datos use azure `https://portal.azure.com/`
Use mi cuenta de Estudiate.

primero cree el grupo de recursos
luego la base de datos `finhogar-database`

/Conexiónes/
Para el workbench segui la guia de conexión 

Para el BackEnd hice lo mismo agregando el certificado ssl `DigiCertGlobalRootCA.crt.pem`
"Todo fue muy rapido practicamente en minutos ya estaba hosteando la base en la nube de azure"
####################################

#BackEnd
Para el backend use vercel `https://vercel.com/`
En este caso al ser un servicio gratuito simplemente me logue con mi cuenta de gitlab `https://gitlab.com/`

/Conexion/
Subi mi repositorio a gitlab y cree el archivo vercel.json
Optimize mi codigo para produccion
Primero segui los pasos de /crear proyecto/eleguir mi repositorio/darle a deploy

Eso seria todo si no me ubiera salido error en la base datos.
Entonce despues un largo tiempo probando muchas configuracion me di cuenta de una configuracion que no hcie a la hora de crear la base de dato, y la de permiter que cualquir ip se conecte a mi base

La aplique y listo ya teniamos la api en la nube junto a la base de datos

/Dependecias Back/
/body-parser/cors/dotenv/express/morgan/mysql2/nodemon/vercel
####################################

#FrontEnd
Para el frontend tambien use vercel `https://vercel.com/`
El Front no me dio ningun problemas a la hora de deploguear

/Conexion/
subi mi repositorio a gitlab
fui a vercel cree proyecto elegui el repo
Vercel detecto que usu vite y le di a deploy 

/Dependecias Front/
/axios/daisyui/react/react-dom/react-router-dom

############################################################################################################

Conculucion Final

Me encanto hacer este proyecto que vengo trabajando hace un buen tiempo
me enamore de react y git, en el backend prefiero python y dejango, pero me voy contento con los conocimientos BackEnd, FrontEnd y de bases de datos
